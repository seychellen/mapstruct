package org.javacook.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface SimpleSourceDestinationMapper {

    @Mapping(target = "nothing", source = "nothing", defaultExpression = "java(source.getName())")
    SimpleDestination sourceToDestination(SimpleSource source);

    SimpleSource destinationToSource(SimpleDestination destination);
}
